library(politicaldata)

pres <- politicaldata::pres_results

pres <- pres %>% 
  filter(year%in% c(2016))

pres$state_name = state.name[match(pres$state,state.abb)] # we need the full name of the state to join with the map

pres <- pres %>% mutate(republican = rep>0.5)
pres <- pres %>% mutate(democratic = dem>0.5) %>% rename(State = "state_name", Polit = "republican") %>% select(State, Polit)

as.factor(pres$polit)
pres$polit <- factor(
  pres$Polit,
  levels = c(FALSE,TRUE),
  labels = c("Republican", "Democratic")
) 

covid_map_polit <- merge(covid_map, pres, by = "State")

library(usmap)
plot_usmap(data = pres, values = "logoddsmis",  color = Polit, labels=TRUE) + 
  scale_fill_continuous( low = "white", high = "red",
                         name = "Misinfo log-odds", label = scales::comma
  ) + theme(legend.position = "bottom", plot.title = element_text(size = 12)) +
  theme(panel.background = element_rect(colour = "black")) + 
  labs(title = "January, 2020", caption = "Source: Bing Coronavirus Query Set")
