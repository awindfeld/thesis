library(tidymodels) # Collection of packages for modeling and ML learning using tidy principles. Successor to caret in a way.
library(textrecipes) # Convert text features into numerical features. Fits into the tidymodels framework
library(stopwords)
library(tidyverse)

# tidymodels view the first level of a factor as the positive
levels(df_coded$misinfo)

# reverse the levels!
df_coded <- df_coded %>% mutate(misinfo = fct_rev(misinfo))

# Split data into train and test set. Strata creates stratified sampling so test and training has equal proportions. 
# It can be argues that this would still have been somewhat evenly stratified as the set was already fairly balanced. 
set.seed(1234)

misinfo_split <- initial_split(df_coded, prop = 9/10, strata = misinfo)
misinfo_split

misinfo_train <- training(misinfo_split)
misinfo_test <- testing(misinfo_split)

# Feature engineering / Pre-processing

misinfo_rec <- 
  recipe(misinfo ~ Query,
         data = misinfo_train
  ) %>% 
  step_tokenize(Query) %>%                                                  # creating text predictors. Tokenization
  step_stopwords(Query) %>%                                                 # removing stopwords (library stopwords)
  step_ngram(Query, num_tokens = 2, min_num_tokens = 1) %>%                 # ngrams
  step_tokenfilter(Query, max_tokens = tune(), min_times = 5) %>%           # max_tokens = tune(). Tuning the number of tokens used in the model
  step_tfidf(Query) 

#Check stuff
#%>% prep() %>% juice()


# Model specification. Using parsnip search to find the needed model. Once again tune() is used select parameter for later tuning

glm_spec <- logistic_reg(penalty = tune(), mixture = 1) %>%
  set_mode("classification") %>%
  set_engine("glmnet")

# possible parameter grid. Try other values

param_grid <- grid_regular(
  penalty(range = c(-4, 0)),
  max_tokens(range = c(100, 500)),
  levels = 10
)

# Cross validation folds

set.seed(123)
misinfo_folds <- vfold_cv(misinfo_train, strata = misinfo)

# Create a workflow/pipeline

glm_wf <- workflow() %>% 
  add_recipe(misinfo_rec) %>% 
  add_model(glm_spec)

# Tuning. resamples, model, and features are all done so models can now be trained. 

library(glmnet)

set.seed(45)

glm_tune <- tune_grid(
  glm_wf,
  resamples = misinfo_folds,
  grid = param_grid, 
  control = control_grid(save_pred = TRUE, verbose = TRUE)
) 


# Collect and evaluate results

# metrics for each model
collect_metrics(glm_tune)

autoplot(glm_tune)

# Best model?
glm_tune %>%
  show_best("roc_auc")

best_roc_auc <- select_best(glm_tune, "roc_auc")
best_roc_auc

collect_predictions(glm_tune, parameters = best_roc_auc) %>% 
  group_by(id) %>% 
  roc_curve(truth = misinfo, .pred_Yes) %>% 
  autoplot()

glm_tune$.predictions

# Finalize with best model
glm_wf_final <- finalize_workflow(glm_wf, best_roc_auc)


# variable importance

library(vip)
library(hrbrthemes)

vi_data <- glm_wf_final %>%
  fit(misinfo_train) %>%
  pull_workflow_fit() %>%
  vi(lambda = best_roc_auc$penalty) %>%
  mutate(Variable = str_remove_all(Variable, "tfidf_Query_")) %>%
  filter(Importance != 0)

# visualize varimp

vi_data %>%
  mutate(
    Importance = abs(Importance)
  ) %>%
  filter(Importance != 0) %>%
  group_by(Sign) %>%
  top_n(30, Importance) %>%
  ungroup() %>%
  mutate(Sign = factor(Sign, c("POS", "NEG"), c("No", "Yes"))) %>%
  ggplot(aes(
    x = Importance,
    y = fct_reorder(Variable, Importance),
    fill = Sign
  )) +
  geom_col(show.legend = FALSE) +
  scale_x_continuous(expand = c(0, 0)) +
  facet_wrap(~Sign, scales = "free") +
  labs(
    y = NULL
  ) + theme_ipsum_pub()


# Using `last_fit()` to fit the model a last time and evaluate it


final_fit <- last_fit(
  glm_wf_final, 
  misinfo_split
)

final_fit


# Metrics of the testing set

final_fit %>%
  collect_metrics()

final_fit %>%
  collect_predictions() %>%
  roc_curve(truth = misinfo, .pred_Yes) %>%
  autoplot()

# confusion matrix

final_fit %>%
  collect_predictions() %>%
  conf_mat(misinfo, .pred_class)

# apply to new data

full_training_fit <- glm_wf_final %>%
  fit(df_coded)

covid_predicted <- covid %>%
  bind_cols(
    predict(full_training_fit, new_data = covid, type = "class"),
    predict(full_training_fit, new_data = covid, type = "prob")
  )

covid_predicted %>% group_by(.pred_class) %>% summarise(count = n()) 

chk <- covid_predicted %>%
  filter(.pred_class == "Yes") %>%
  group_by(Query) %>%
  count(sort = TRUE) 
