---
title: "Appendix A - R Code, Tables, & Visualizations"
output:
  pdf_document:
    toc: true
    toc_depth: 3
    number_sections: true
---



# Initial setup. Loading packages and data. 


```{.r .watch-out}
library(tidyverse)
library(lubridate)
library(scales)
library(knitr)
library(kableExtra)
library(tidylo)
library(tidytext)
library(hrbrthemes)
library(ggthemes)
library(extrafont)

extrafont::loadfonts(device="win")
```

Download data from the Microsoft Github and create full dataset
Credit to Christopher Yee for the code: https://www.christopheryee.org/blog/exploratory-data-analysis-on-covid-19-search-queries/


```{.r .watch-out}
# Download the tsv files from Microsoft
jan <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-01-01_2020-01-31.tsv")
feb <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-02-01_2020-02-29.tsv")
mar <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-03-01_2020-03-31.tsv")
apr <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-04-01_2020-04-30.tsv")
may <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-05-01_2020-05-31.tsv")
jun <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-06-01_2020-06-30.tsv")
jul <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-07-01_2020-07-31.tsv")
aug <- read_tsv("https://raw.githubusercontent.com/microsoft/BingCoronavirusQuerySet
                /master/data/2020/QueriesByState_2020-08-01_2020-08-31.tsv")

# Join months to create a data set containing everything
df_compile <- rbind(jan, feb, mar, apr, may, jun, jul, aug)

# Save data as csv
df_compile %>% 
  write_csv("coronavirus-queriesbyState.csv")

# Remove files from workspace
rm(df_compile, jan, feb, mar, apr, may, jun, jul, aug)
```

## Loading data

Microsoft Bing search query logs


```{.r .watch-out}
# Load data from csv
covid_raw <- read_csv("coronavirus-queriesbyState.csv")
covid_raw <- covid_raw %>% mutate(Month = floor_date(Date, "month"))
head(covid_raw, 10)
```

```
## # A tibble: 10 x 7
##    Date       Query                IsImplicitIntent State                        Country        PopularityScore Month     
##    <date>     <chr>                <lgl>            <chr>                        <chr>                    <dbl> <date>    
##  1 2020-01-01 coronavirus          FALSE            Michigan                     United States                1 2020-01-01
##  2 2020-01-01 coronavirus          FALSE            Pennsylvania                 United States                1 2020-01-01
##  3 2020-01-01 p2 masks             TRUE             Australian Capital Territory Australia                    1 2020-01-01
##  4 2020-01-01 auswärtiges amt      TRUE             North Rhine-Westphalia       Germany                      1 2020-01-01
##  5 2020-01-01 p2 masks             TRUE             New South Wales              Australia                    1 2020-01-01
##  6 2020-01-02 coronavirus          FALSE            California                   United States               67 2020-01-01
##  7 2020-01-02 webasto              TRUE             North Rhine-Westphalia       Germany                      1 2020-01-01
##  8 2020-01-02 coronavirus          FALSE            Ohio                         United States                1 2020-01-01
##  9 2020-01-02 corona virus         FALSE            California                   United States              100 2020-01-01
## 10 2020-01-02 arrowe park hospital TRUE             Merseyside                   United Kingdom             100 2020-01-01
```

Coronanet data including political interventions


```{.r .watch-out}
covid_pol_core <- read_csv("coronanet_core.csv")
head(covid_pol_core, 10)
```

```
## # A tibble: 10 x 38
##    record_id policy_id recorded_date       date_updated date_announced date_start date_end   entry_type update_type event_descripti~ domestic_policy
##    <chr>         <dbl> <dttm>              <date>       <date>         <date>     <date>     <chr>      <chr>       <chr>                      <dbl>
##  1 1000140Et   1000140 2020-06-20 19:07:14 2020-06-20   2020-03-24     2020-03-24 NA         new_entry  <NA>        "The Katsina St~               1
##  2 1000140Es   1000140 2020-06-20 19:07:14 2020-06-20   2020-03-24     2020-03-24 NA         new_entry  <NA>        "The Katsina St~               1
##  3 1000509Dw   1000509 2020-06-17 12:53:43 2020-06-17   2020-03-19     2020-03-19 2020-04-27 update     End of Pol~ "On March 19, t~               1
##  4 1000509Dx   1000509 2020-06-17 12:53:43 2020-06-17   2020-03-19     2020-03-19 2020-04-27 update     End of Pol~ "On March 19, t~               1
##  5 1000509Co   1000509 2020-06-17 12:53:43 2020-06-17   2020-03-19     2020-03-19 2020-04-27 update     End of Pol~ "On March 19, t~               1
##  6 1000775Eo   1342691 2020-04-04 15:43:15 2020-04-04   2020-03-14     2020-03-14 NA         update     <NA>        "Chile extends ~               0
##  7 1000775Eb   1342691 2020-04-04 15:43:15 2020-04-04   2020-03-14     2020-03-14 NA         update     <NA>        "Chile extends ~               0
##  8 1003875NA   1003875 2020-04-15 11:04:50 2020-04-15   2020-04-07     2020-04-07 NA         new_entry  <NA>        "On April 7th, ~               1
##  9 1004103NA   1004103 2020-05-15 11:04:46 2020-05-15   2020-05-15     2020-05-15 NA         new_entry  <NA>        "The total numb~               1
## 10 100452Cx     100452 2020-05-31 22:47:59 2020-05-31   2020-03-28     2020-03-28 NA         new_entry  <NA>        "An Egyptian pa~               1
## # ... with 27 more variables: type <chr>, type_sub_cat <chr>, type_text <dbl>, index_high_est <dbl>, index_med_est <dbl>, index_low_est <dbl>,
## #   index_country_rank <dbl>, correct_type <chr>, country <chr>, init_country_level <chr>, province <chr>, city <chr>, source_corr_type <chr>,
## #   target_country <chr>, target_geog_level <chr>, target_region <chr>, target_province <chr>, target_city <chr>, target_other <lgl>,
## #   target_who_what <chr>, target_direction <chr>, travel_mechanism <chr>, compliance <chr>, enforcer <chr>, link <chr>, ISO_A3 <chr>, ISO_A2 <chr>
```

```{.r .watch-out}
covid_pol_ext <- read_csv("coronanet_extended.csv")
head(covid_pol_ext, 10)
```

```
## # A tibble: 10 x 75
##    date_start country confirmed_cases deaths recovered record_id policy_id recorded_date       date_updated date_announced date_end   entry_type
##    <date>     <chr>             <dbl>  <dbl>     <dbl> <chr>         <dbl> <dttm>              <date>       <date>         <date>     <chr>     
##  1 2020-01-22 Afghan~               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  2 2020-01-22 Albania               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  3 2020-01-22 Algeria               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  4 2020-01-22 Andorra               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  5 2020-01-22 Angola                0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  6 2020-01-22 Antigu~               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  7 2020-01-22 Argent~               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  8 2020-01-22 Armenia               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
##  9 2020-01-22 Austra~               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
## 10 2020-01-22 Austria               0      0         0 <NA>             NA NA                  NA           NA             NA         <NA>      
## # ... with 63 more variables: update_type <chr>, event_description <chr>, domestic_policy <dbl>, type <chr>, type_sub_cat <chr>, type_text <lgl>,
## #   index_high_est <dbl>, index_med_est <dbl>, index_low_est <dbl>, index_country_rank <dbl>, correct_type <chr>, init_country_level <chr>,
## #   province <chr>, city <chr>, source_corr_type <chr>, target_country <chr>, target_geog_level <chr>, target_region <chr>, target_province <chr>,
## #   target_city <chr>, target_other <lgl>, target_who_what <chr>, target_direction <chr>, travel_mechanism <chr>, compliance <chr>, enforcer <chr>,
## #   link <chr>, ISO_A3 <chr>, ISO_A2 <chr>, tests_daily_or_total <chr>, tests_raw <dbl>, ccode <dbl>, ifs <chr>, pop_WDI_PW <dbl>, gdp_WDI_PW <dbl>,
## #   gdppc_WDI_PW <dbl>, growth_WDI_PW <dbl>, lnpop_WDI_PW <dbl>, lngdp_WDI_PW <dbl>, lngdppc_WDI_PW <dbl>, FarRight_IO <dbl>,
## #   ExternalLaborOpenness_IO <dbl>, eco_glob_KOF <dbl>, soc_glob_KOF <dbl>, cult_prox_KOF <dbl>, poli_glob_KOF <dbl>, overallGlob_index_KOF <dbl>,
## #   news_WB <lgl>, disap_FA <dbl>, polpris_FA <dbl>, latentmean_FA <dbl>, transparencyindex_HR <dbl>, state_IDC <dbl>, muni_IDC <dbl>,
## #   dispersive_IDC <dbl>, constraining_IDC <dbl>, inclusive_IDC <dbl>, Rank_FP <dbl>, Score_FP <dbl>, sfi_SFI <dbl>, ti_cpi_TI <dbl>,
## #   v2x_polyarchy_VDEM <dbl>, EmigrantStock_EMS <dbl>
```


## Distribution of queries


```{.r .watch-out}
month_dist <- covid_raw %>%
  select(Country, Month) %>%
  group_by(Month) %>% 
  count() %>% 
  ungroup() %>% 
  rename("# Queries" = n)
```


```{.r .watch-out}
# Total queries per month
  kbl(month_dist[1:8, 1:2], booktabs = T) %>% 
  kable_styling(latex_options = "striped",font_size = 11) %>% 
  column_spec(1, width = "6cm")
```

<table class="table" style="font-size: 11px; margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;"> Month </th>
   <th style="text-align:right;"> # Queries </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-01-01 </td>
   <td style="text-align:right;"> 39936 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-02-01 </td>
   <td style="text-align:right;"> 126354 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-03-01 </td>
   <td style="text-align:right;"> 892412 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-04-01 </td>
   <td style="text-align:right;"> 775233 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-05-01 </td>
   <td style="text-align:right;"> 501197 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-06-01 </td>
   <td style="text-align:right;"> 502283 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-07-01 </td>
   <td style="text-align:right;"> 547291 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> 2020-08-01 </td>
   <td style="text-align:right;"> 445578 </td>
  </tr>
</tbody>
</table>


```{.r .watch-out}
covid_raw %>% 
  group_by(Month) %>% 
  count() %>% 
  ungroup() %>%   
  ggplot(aes(Month, n)) +
  geom_point() +
  geom_line() +
  scale_y_continuous(labels = comma_format()) +
  theme_minimal() +
  labs(x = NULL, y = NULL,
       title = "Count of COVID-19 search queries per month",
       caption = "Worldwide\nSource: Bing Coronavirus Query Set")
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12-1.png)



```{.r .watch-out}
#Country distribution
country_dist <- covid_raw %>%
  select(Country, Query) %>%
  group_by(Country) %>% 
  count(sort = TRUE) %>% 
  rename("# Queries" = n)
```

Top 10 countries by number of queries


```{.r .watch-out}
# Top 10 countries table
  kbl(country_dist[1:10, 1:2], booktabs = T) %>% 
  kable_styling(latex_options = "striped", font_size = 11) %>% 
  column_spec(1, width = "6cm")
```

<table class="table" style="font-size: 11px; margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;"> Country </th>
   <th style="text-align:right;"> # Queries </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;width: 6cm; "> United States </td>
   <td style="text-align:right;"> 1751769 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> United Kingdom </td>
   <td style="text-align:right;"> 805201 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> France </td>
   <td style="text-align:right;"> 197275 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> Italy </td>
   <td style="text-align:right;"> 172992 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> Germany </td>
   <td style="text-align:right;"> 166362 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> Canada </td>
   <td style="text-align:right;"> 144528 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> Japan </td>
   <td style="text-align:right;"> 78967 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> Spain </td>
   <td style="text-align:right;"> 73105 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> Australia </td>
   <td style="text-align:right;"> 71794 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> India </td>
   <td style="text-align:right;"> 49231 </td>
  </tr>
</tbody>
</table>

Looking at string lenghts


```{.r .watch-out}
covid_raw$query_length = str_length(covid_raw$Query)
mean(covid_raw$query_length)
```

```
## [1] 21.97946
```

Random sample of queries from the United States.


```{.r .watch-out}
set.seed(1234)
query_example <- covid_raw %>% filter(Country == "United States")  %>% sample_n(10) %>% select(Query, IsImplicitIntent,PopularityScore)
```

```{.r .watch-out}
kbl(query_example, booktabs = T) %>% 
  kable_styling(latex_options = "striped", font_size = 11) %>% 
  column_spec(1, width = "6cm")
```

<table class="table" style="font-size: 11px; margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;"> Query </th>
   <th style="text-align:left;"> IsImplicitIntent </th>
   <th style="text-align:right;"> PopularityScore </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;width: 6cm; "> corona virus cases by state in usa </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> china coronavirus </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> aruba coronavirus </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> crocs healthcare </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> umc covid testing las vegas </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> lysol </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> coronavirus symptoms in humans </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> coronavirus disease 2019 prevention </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> dewine </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 6cm; "> covid 19 symptoms </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
</tbody>
</table>


## Filtering country and creating new columns for ID and Month.


```{.r .watch-out}
# Select country
covid <- covid_raw %>% 
  filter(Country == 'United States') %>% 
  mutate(ID = row_number()) %>% 
  mutate(Month = floor_date(Date, "month")) %>% 
  select(ID, everything())
```

## Distribution of COVID-19 searches in the US by populary score


```{.r .watch-out}
covid %>% 
  ggplot() +
  geom_histogram(aes(PopularityScore, fill = IsImplicitIntent), color = 'white') +
  scale_y_continuous(labels = comma_format()) +
  theme_minimal()  +
  theme(legend.position = 'top') +
  labs(x = "Popularity Score", y = NULL,
       title = "Distribution of COVID-19 search queries by Bing's popularity score",
       caption = "United States\nSource: Bing Coronavirus Query Set")
```

![plot of chunk unnamed-chunk-18](figure/unnamed-chunk-18-1.png)


# Data for manual coding
## Create keyword index, create misinfo variable and sample data for manual coding.

Keyword index


```{.r .watch-out}
misinfo_keywords <- "\\b(hydroxychloroquine|hydroxy|chloro|chlori|lupus|exercise|shoes|bacteria|intoxication|oxygen|co2|alcohol|
thermal|scanner|drugs|pepper|fly|flies|houseflies|bleach|chlorine|methanol|ethanol|radiowaves|5g|mobile network|
temperature|for life|forever|hold breath|humid|cold|snow|bath|mosquito|bites|hand dry|blow dry|uv|uv light|light|ultraviolet|
pneumonia vaccine|rinsing nose|saline|salt water|garlic|immunity|antibiotics|coronavirus medicine|covid medicine|corona medicine|
lab|laboratory|chinese|spies|hiv|engineered|predict|simulation|bill gates|gates|patented|manmade|bio|biological|bioweapon|
weapon|5g|cell phone|ozone|colloidal|silver solution|miracle|mineral|vitamin|lemon|hot water|migrants|microchip|flu shot|hypercapnia|
carbon dioxide|brain|george soros|patented|euthanasia|contact tracing|profit|alkaline|helicopter|chemical|polio|leak|whistleblower|
whistle|resistance|ethnicity|xenophobic|vegetarian|qanon|hoax|lie|deep state|elites|simpsons|cloud|snake|pet animal|pets|rabies|
livestock|herd|eggs|poultry|cookies|rice|red bull|bat soup|oranges|imported|notes|vinegar|rose water|urine|warm socks|mustad patch|
goose fat|moist|spicy food|hot food|vitamin|cold drink|preserved food|ice cream|milkshake|sesame|granite|lettuce|centella|
 mint |white willow|saffron|turmeric|cinnamon|petroleum|diy|do-it-yourself|self diagnose|cannabis|clothe|planned|war|
 jew|terrorism|terrorist|terror|zionist|israel|scheme|tom cotton|rush limbaugh|asians|chinese|uncivilized|bioterror|
 yellow alert|always china|chinese virus|wuhan virus|chinavirus|wuhanvirus|malaria|antibio|military|made|fake|conspiracy|lieber)\\b"
```

Use index to create new boolean variable for misinfo status


```{.r .watch-out}
covid_misinf <- covid %>% mutate(misinf_query = str_detect(Query, regex(misinfo_keywords, ignore_case = TRUE)))
```

Top represented keywords


```
## # A tibble: 378 x 2
## # Groups:   Query [378]
##    Query                              n
##    <chr>                          <int>
##  1 qanon                           4685
##  2 herd immunity                   1641
##  3 hydroxychloroquine coronavirus  1402
##  4 face masks made in usa          1400
##  5 bill gates coronavirus           991
##  6 masks made in usa                685
##  7 malaria drug for coronavirus     679
##  8 wuhan virus                      662
##  9 diy hand sanitizer               505
## 10 bat soup                         402
## # ... with 368 more rows
```


```{.r .watch-out}
# Top 10 countries table
  kbl(covid_misinf[1:10, 1:2], booktabs = T) %>% 
  kable_styling(latex_options = "striped", font_size = 11) %>% 
  column_spec(1, width = "6cm")
```

<table class="table" style="font-size: 11px; margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:right;"> ID </th>
   <th style="text-align:left;"> Date </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;width: 6cm; "> 1 </td>
   <td style="text-align:left;"> 2020-01-01 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 2 </td>
   <td style="text-align:left;"> 2020-01-01 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 3 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 4 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 5 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 6 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 7 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 8 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 9 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
  <tr>
   <td style="text-align:right;width: 6cm; "> 10 </td>
   <td style="text-align:left;"> 2020-01-02 </td>
  </tr>
</tbody>
</table>

Distribution of information/misinformation


```{.r .watch-out}
# check distribution
covid_misinf %>% group_by(misinf_query) %>% summarise(count = n()) 
```

```
## # A tibble: 2 x 2
##   misinf_query   count
##   <lgl>          <int>
## 1 FALSE        1731428
## 2 TRUE           20341
```

