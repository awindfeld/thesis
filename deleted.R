# Unnest
tidy_covid <- covid %>% 
  unnest_tokens(word, Query) %>%
  group_by(word) %>% 
  select(ID, everything())

#bigrams
tidy_bigram <- covid %>%
  unnest_tokens(bigram, Query, token = "ngrams", n = 2) %>% 
  group_by(bigram) %>% 
  select(ID, everything())

# Create binary class with misinformation status
covid_misinf <- tidy_covid %>% 
  mutate(misinf_query = ifelse(word %in% misinfo_keywords, 1,0))

# check distribution
covid_misinf %>% group_by(misinf_query) %>% summarise(count = n())  

# Top misinf keywords?
chk <- covid_misinf %>% 
  filter(misinf_query == 1) %>% 
  group_by(word) %>% 
  count(sort = TRUE)


# ...
misinfo_keywords <- "\\b(hydroxy|chloro|chlori|malaria|hydroxy|chloro|chlori|malaria|lupus|gates|shoes|5g|hold breath|bio
|qanon|salt water|saline|china lab|chinese|chinavirus|laboratory|alcohol|garlic|mosquito|uv light|uv|ultraviolet|violet|pneumonia|antibiotics|
snake|pets|cold|vinegar|silver solution|vitamin|cannabis|clothe|weapon|military|ozone|humid|drink|temperature|blow dry|rinsing nose|antibio|
wuhanvirus|wuhan virus|china virus)\\b"

#Keywords related to misinformation
misinfo_keywords <- c('hydroxy','chloro', 'chlori','malaria','lupus','gates','shoes','5g','hold breath','bio','qanon','salt water','saline','china lab',
                      'chinese','chinavirus','laboratory', 'alcohol','garlic','mosquito','light','uv','ultraviolet','violet','pneumonia','antibiotics',
                      'snake','pets','cold','vinegar','silver solution','vitamin','cannabis','clothe','weapon','military','ozone','humid','drink',
                      'temperature', 'blow dry','rinsing nose','antibio','wuhanvirus', 'wuhan virus', 'china virus')
