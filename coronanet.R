covid_map <- covid_predicted

# making various new columns including logodds
covid_map  <- covid_map %>% group_by(State) %>% 
  summarise(n = n(), nmis = sum(.pred_class == "Yes")) %>% mutate(pmis = (nmis / n), logoddsmis = log(pmis / (1-pmis)))

covid_map$fips <- fips(covid_map$State)
covid_map <- covid_map %>% rename(total = "n")

# Type - Days in effect
covid_qua <- covid_pol_core %>%
  filter(type %in% c("Quarantine", "Social Distancing", "Lockdown", "Declaration of Emergency",
                     "Restriction and Regulation of Government Services", "Restrictions of Mass Gatherings"),
         country == "United States of America",
         str_sub(compliance, 1,9)== "Mandatory")

# covid_qua <- covid_pol_core %>%
#   filter(
#          country == "United States of America",
#          str_sub(compliance, 1,9)== "Mandatory")

covid_qua <- covid_qua %>% 
  select(date_start, date_end, State = province, type, description, compliance) 

covid_qua <- covid_qua %>%  
  mutate(days = if_else(is.na(date_end), ymd("2020-08-31"), date_end)-date_start)
  
covid_qua <- covid_qua %>% group_by(type, State) %>% summarise(days_total = sum(days)) #%>% ggplot(aes(State, days_total)) + geom_col() + coord_flip()

covid_qua2 <- covid_map%>% left_join(covid_qua)

covid_qua2 %>% filter (!is.na(type)) %>% ggplot(aes(days_total, pmis)) + geom_point(size = 1.5, alpha = 0.8) +
  scale_y_continuous(labels = scales::percent)+
  geom_text(aes(label=ifelse(pmis>0.015 | days_total>800,as.character(State),'')),vjust="inward",hjust="inward", color = "black") + 
  labs(title = "Political Interventions and Misinformation", subtitle = "January - August, 2020",
       caption = "Source: Bing Coronavirus Query Set & \nCoronanet Database Version 1.0 (Core)",
       y = "Misinformation Percent", x = "Cumulative Days with Intervention in Effect") + facet_wrap(~type) + theme_bw() + theme(legend.position = "none")

# counts

covid_pol_count <- covid_pol_core %>% filter(country == "United States of America") %>% rename(State = province)

covid_pol_count <- covid_pol_count %>% group_by(State) %>% summarise(count = n())

covid_map_count <- covid_map %>% left_join(covid_pol_count)

covid_map_count %>% ggplot(aes(count, pmis))+ 
  geom_point() +
  scale_y_continuous(labels = scales::percent)+
  geom_text(aes(label=ifelse(pmis>0.015 | count>110,as.character(State),'')),vjust="inward",hjust="inward", color = "black") + 
  labs(title = "Total Political Interventions (Count) and Misinformation", 
       subtitle = "January - August, 2020", caption = "Source: Bing Coronavirus Query Set & \nCoronanet Database Version 1.0 (Core)",
      x = "Count of Political Interventions", y = "Misinformation Percent") + theme_bw()

library(usmap)
plot_usmap(data = covid_map_count, values = "count",  color = "#50486D", labels=TRUE) + 
  scale_fill_continuous( low = "white", high = "blue",
                         name = "Misinfo Percent", 
  ) + theme(legend.position = "bottom", plot.title = element_text(size = 12)) +
  theme(panel.background = element_rect(colour = "black")) + 
  labs(title = "August, 2020", caption = "Source: Bing Coronavirus Query Set") +


### Compliance
# Type - Days in effect
covid_qua <- covid_pol_core %>%
  filter(country == "United States of America",
         str_sub(compliance, 1,9)== "Mandatory")

covid_qua <- covid_qua %>% 
  select(date_start, date_end, State = province, description, compliance) 

covid_qua <- covid_qua %>%  
  mutate(days = if_else(is.na(date_end), ymd("2020-08-31"), date_end)-date_start)

covid_qua <- covid_qua %>% group_by(State) %>% summarise(count = n()) #%>% ggplot(aes(State, days_total)) + geom_col() + coord_flip()

covid_qua2 <- covid_map%>% left_join(covid_qua)

covid_qua2 %>% ggplot(aes(count, pmis)) + geom_point() +
  geom_text(aes(label=ifelse(pmis>0.015 | count>150,as.character(State),'')),vjust="inward",hjust="inward", color = "black") 


