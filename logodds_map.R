
library(usmap)
covid_map <- covid_predicted
covid_map  <- covid_map %>% group_by(State) %>% 
  summarise(n = n(), nmis = sum(.pred_class == "Yes")) %>% mutate(pmis = (nmis / n), logoddsmis = log(pmis / (1-pmis)))

library(tidycensus)
options(scipen = 999)
#tidycensus::census_api_key(key =  "84445bc1774125cc0aabe93cff9471055e92650d", install = T)

us_components <- get_estimates(geography = "state", product = "population")
us_components <- us_components %>% filter(variable == "POP") %>% rename(State = "NAME",Population = "value") %>%  select(State, Population)
covid_map <- merge(covid_map, us_components, by = "State")
covid_map <- covid_map %>% mutate(pmis_pop = (nmis / Population), logoddsmispop = log(pmis_pop / (1-pmis_pop))) 
covid_map <- covid_map %>% mutate(nreg = (total - nmis)) %>% relocate(nreg, .after = total)
cor(covid_map$nreg, covid_map$nmis, method = c("spearman"))
covid_map$fips <- fips(covid_map$State)
covid_map <- covid_map %>% rename(total = "n")

plot_usmap(data = covid_map, values = "pmis",  color = "#50486D", labels=TRUE) + 
  scale_fill_continuous( low = "white", high = "red",
                         name = "Misinfo log-odds", label = scales::comma
  ) + theme(legend.position = "bottom", plot.title = element_text(size = 16, face = "bold")) +
  theme(panel.background = element_rect(colour = "black")) + 
  labs(title = "Misinformation Relative to Regular Queries (Log Odds), By State, January - August, 2020", caption = "Source: Bing Coronavirus Query Set")


# Relationship between total queries and population
covid_map %>% ggplot(aes(Population, nmis, color = "orange")) + 
  geom_point(size = 3,alpha = 0.6) + 
  geom_smooth(method = "loess", span = 1, color = "darkblue", alpha = 0.3) +
  scale_x_continuous(labels = label_number(scale = 1/1000000)) +
  scale_y_continuous(labels = label_comma()) +
  labs(x = "Population (Millions)", y = "Total Queries",
       title = "Relationship Between State Population & Total Queries",
       caption = "Source: Bing Coronavirus Query Set \n& United States Cencus Bureau" ) + theme_bw() + theme (legend.position = "none") +
  geom_text(aes(label=ifelse(Population>15000000,as.character(State),'')),hjust=1.1,vjust=-0.2, color = "black")



covid_map %>% ggplot(aes(nreg, nmis, color = "orange")) + 
  geom_point(size = 3,alpha = 0.6) + 
  geom_smooth(formula = y ~ x-1, color = "darkblue", alpha = 0.3) +
  scale_x_continuous(labels = label_number()) +
  scale_y_continuous(labels = label_comma()) +
  labs(x = "Regular Queries", y = "Misinformation Queries",
       title = "Relationship Between Regular Information & Misinformation", subtitle = "Labelled by State",
       caption = "Source: Bing Coronavirus Query Set" ) + theme_bw() + theme (legend.position = "none") +
  geom_label(aes(label=ifelse(nmis>0, as.character(State),'')),hjust=1.08,vjust=-0.25, color = "black")



  