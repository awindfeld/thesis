library(ggpubr)

scale_limit <- range(c(feb$pmis, mar$pmis, apr$pmis,
                       may$pmis, jun$pmis, jul$pmis, aug$pmis))

#February
p_feb_2 <- plot_usmap(data = feb, values = "pmis",  color = "#50486D", labels=TRUE) + 
  scale_fill_continuous( low = "white", high = "blue",
                         name = "Misinfo Percent", label = scales::percent, limits = scale_limit
  ) + theme(legend.position = "bottom", plot.title = element_text(size = 12)) +
  theme(panel.background = element_rect(colour = "black")) + 
  labs(title = "February, 2020", caption = "Source: Bing Coronavirus Query Set")


#May
p_may_2 <- plot_usmap(data = may, values = "pmis",  color = "#50486D", labels=TRUE) + 
  scale_fill_continuous( low = "white", high = "blue",
                         name = "Misinfo Percent", label = scales::percent, limits = scale_limit
  ) + theme(legend.position = "bottom", plot.title = element_text(size = 12)) +
  theme(panel.background = element_rect(colour = "black")) + 
  labs(title = "May, 2020", caption = "Source: Bing Coronavirus Query Set")


#Aug
p_aug_2 <- plot_usmap(data = aug, values = "pmis",  color = "#50486D", labels=TRUE) + 
  scale_fill_continuous( low = "white", high = "blue",
                         name = "Misinfo Percent", label = scales::percent, limits = scale_limit
  ) + theme(legend.position = "bottom", plot.title = element_text(size = 12)) +
  theme(panel.background = element_rect(colour = "black")) + 
  labs(title = "August, 2020", caption = "Source: Bing Coronavirus Query Set")

map_months_2 <- ggarrange(p_feb_2, p_may_2, p_aug_2, common.legend = TRUE, legend = "bottom",
                        font.label = list(size = 10, color = "grey"),
                        ncol = 1, nrow = 3) + theme_minimal() 

annotate_figure(map_months_2,
                top = text_grob("Misinformation Percentage Change (Feb/May/Aug)", color = "black", face = "bold", size = 14))


